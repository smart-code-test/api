**Lenvantar el servidor<br>

1.Clonar el repositorio: https://gitlab.com/smart-code-test/api.git<br>
2.Abrir el terminal en la carpeta donde se clonó el proyecto<br>
3.Ejecutar yarn install<br>
4.Cargar el script SQL y ejecutar MySQL en el puerto 3306 (para el desarrollo se utilizó XAMP)<br>
5.Ejecutar npm run server en terminal<br>

versión de yarn utilizada en el desarrollo: 1.22.4