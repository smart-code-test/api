const mysql = require('mysql');

const mysqlConnection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'smart_test'
});

mysqlConnection.connect(function (err) {
	if(err){
		console.log(err);
		console.log('DB connexion failure');
		return;
	}else{
		console.log('DB connexion successful');
	}
});

module.exports = mysqlConnection;