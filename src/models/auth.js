const db = require('../db');

const login = (mail) => {
	return new Promise ((resolve,reject) =>{
		db.query('SELECT * FROM users WHERE mail = ?',mail,(error,rows) => {
			if(!error) resolve(rows[0]);
			reject(error);
		});
	});
};

const getById = (id) => {
	return new Promise ((resolve,reject) =>{
		db.query('SELECT id, id_user_type, mail, name FROM users WHERE id = ?',id,(error,rows) => {
			if(!error) resolve(rows[0]);
			reject(error);
		});
	});
};

module.exports = {
	login: login,
	getById: getById
}