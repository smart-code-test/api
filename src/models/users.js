const db = require('../db');

const signUp = (data) => {
	return new Promise ((resolve,reject) => {
		db.query('INSERT INTO users (id_user_type, name, mail, pass) VALUES (?,?,?,?)',[data.id_user_type,data.name,data.mail,data.pass],(error,result) => {
			if(!error) resolve({msj: 'Usuario registrado satisfactoriamente'});
			reject(error);
		});
	});
};

module.exports = {
	signUp: signUp
}