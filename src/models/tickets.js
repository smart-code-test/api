const db = require('../db');

const getAll = () => {
	return new Promise ((resolve, reject) => {
		db.query('SELECT tickets.id, name, mail FROM tickets INNER JOIN users ON tickets.id_user = users.id WHERE ticket_requested = "0";', (error, rows) => {
			if(!error) resolve(rows);
			reject(error);
		});
	});
};

const selectByUser = (id_user) => {
	return new Promise ((resolve,reject) => {
		db.query('SELECT * FROM tickets WHERE id_user = ?',[id_user],(error,rows) => {
			if(!error) resolve(rows);
			reject(error);
		});
	});
};

const request = (data) => {
	return new Promise ((resolve,reject) => {
		db.query('INSERT INTO tickets SET ?',data,(error,rows) => {
			if(!error) resolve({Status: 'Ticket solicitado'});
			reject(error);
		});
	});
};

const assign = (id_ticket) => {
	return new Promise ((resolve,reject) => {
		db.query('UPDATE tickets SET ticket_requested= "1" WHERE id = ?',id_ticket,(error,rows) => {
			if(!error) resolve({Status: 'Ticket asignado'});
			reject(error);
		});
	});
};

const _delete = (id_ticket) => {
	return new Promise ((resolve,reject) => {
		db.query('DELETE FROM tickets WHERE id = ?',id_ticket,(error,rows) =>{
			if(!error) resolve({Status: 'Ticket eliminado'});
			reject(error);
		});
	});
};

module.exports = {
	getAll: getAll,
	selectByUser: selectByUser,
	request: request,
	assign: assign,
	delete: _delete
}