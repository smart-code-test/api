function guard(role,roles) {
	if(role === 'admin'){
		if(!roles.includes(role)) return false;
	}
	if(role === 'user'){ 
		if(!roles.includes(role)) return false;
	}
	return true;
}

module.exports = {
	guard: guard
}