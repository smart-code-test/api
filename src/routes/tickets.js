const express = require('express');
const router = express.Router();
const access = require('../services/auth');
const middleware = require('./middleware');
router.use(middleware.checkToken);

const Tickets = require('../models/tickets');
const url = '/api/tickets';
//admin
router.get(url, async (req,res) =>{
	const tickets = await Tickets.getAll();
	const accessGuard = access.guard(req.userRole,['admin']);
	if(accessGuard){
		res.json(tickets);
	}else{
		res.json({Message: 'Access denied'})
	}
});
//user
router.get(url+'/user/:id_user', async (req,res) => {
	const { id_user } = req.params;
	const tickets = await Tickets.selectByUser(id_user);
	res.json(tickets);
});
//user
router.post(url+'/request', async (req,res) => {
	const data = {
		id_user: req.body.id_user,
		ticket_requested: '0'
	}
	const request = await Tickets.request(data);
	res.json(request);
});
//admin
router.put(url+'/:id_ticket', async (req,res) => {
	const { id_ticket } = req.params;
	const assignment = await Tickets.assign(id_ticket);
	const accessGuard = access.guard(req.userRole,['admin']);
	if(accessGuard){
		res.json(assignment);
	}else{
		res.json({Message: 'Access denied'})
	}
});
//admin
router.delete(url+'/:id_ticket', async (req,res) => {
	const { id_ticket } = req.params;
	const exclusion = await Tickets.delete(id_ticket);
	const accessGuard = access.guard(req.userRole,['admin']);
	if(accessGuard){
		res.json(exclusion);
	}else{
		res.json({Message: 'Access denied'})
	}
});

module.exports = router;