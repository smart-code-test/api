const jwt = require('jwt-simple');
const moment = require('moment');


const checkToken = (req,res,next) => {
	let token = req.headers['authorization'];
	let data = null;
	let role = null;

	if(token !== undefined){
		token = token.split(' ')[1];
	}else{
		token = false;
	}

	if(!token) return res.json({error: 'Token not provided'});
	
	try{
		data = jwt.decode(token, "auth-token");
	}catch(error){
		return res.json({error: 'Invalid token'});
	}

	if(moment().unix() > data.expiresAt){
		return res.json({error: 'Expired token'});
	}

	req.userId = data.id;
	req.userRole = checkRole(data.role);

	next();
};

const checkRole = (role) => { 	
	if(role === 1){
		return "admin";
	}else{
		return "user";
	}
};

module.exports = {
	checkToken: checkToken
}