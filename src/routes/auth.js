const express = require('express');
const router = express.Router();
const Auth = require('../models/auth');
const middleware = require('./middleware');

//encrypt pass
const bcrypt = require('bcrypt');
//create token
const jwt = require('jwt-simple');
const moment = require('moment');
const url = '/api/auth';

router.post(url+'/login', async (req,res) => {
	const user = await Auth.login(req.body.mail);
	if(user === undefined) {
		res.json({error: 'Correo o contraseña incorrecta'});
	}else{
		const password = bcrypt.compareSync(req.body.pass, user.pass);
		if(!password){
			res.json({error: 'Correo o contraseña incorrecta'});
		}else{
			res.json({
				token: createToken(user),
				user_type: checkUserType(user.id_user_type)
			});
		}
	}
});

const checkUserType = (type) => {
	return type === 1 ? 'admin' : 'user'
}

router.use(middleware.checkToken);

router.get(url, async (req,res) => {
	const auth = await Auth.getById(req.userId);
	res.json(auth);
});

const createToken = (user) => {
	let data = {
		id: user.id,
		role: user.id_user_type,
		createdAt: moment().unix(),
		expiresAt: moment().add(1, 'day').unix()
	}
	return jwt.encode(data, "auth-token");
};

module.exports = router;