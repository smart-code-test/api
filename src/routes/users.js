const express = require('express');
const router = express.Router();
//encrypt pass
const bcrypt = require('bcrypt');

const Auth = require('../models/auth');
const Users = require('../models/users');
const url = '/api/user';

router.post(url+'/sign_up', async (req,res) =>{
	console.log(req.body);
	const user = await Auth.login(req.body.mail);
	if(!user){
		req.body.pass = bcrypt.hashSync(req.body.pass, 10);
		const register = await Users.signUp(req.body);
		res.json(register);
	}else{
		res.json({msj: 'Usuario existente'});
	}
});

module.exports = router;