const express = require('express');
const app = express();
const cors = require('cors');


//Config
app.set('port', 3001)

//Middlewares
app.use(express.json());
app.use(cors());

//Routes
app.use(require('./routes/users'));
app.use(require('./routes/auth'));
app.use(require('./routes/tickets'));

//Server
app.listen(app.get('port'), () =>{
	console.log('Server listen on port', app.get('port'));
})
